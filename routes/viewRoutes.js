const express = require('express')
const router = express.Router()
const viewController = require('./../controller/viewController')


router.get('/', viewController.getHome)
router.get('/login',viewController.getLoginForm)
router.get('/signup', viewController.getSignForm)

module.exports = router